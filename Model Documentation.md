# CarND-Path-Planning-Project
Self-Driving Car Engineer Nanodegree Program

## Behavior Planning
In this part, the goal is to predict which state is the best state for the car. By saying best, it means both safety and efficiency. On the safty side, the car should be keep a distance from other cars, no matter it's keeping lane or changing lane. For efficiency, the car should stay in a lane where the traffic flows fast enough. 

Here I model the behavior of the car to be five states-Staying in the left lane, the middle lane, the right lane, left lane change, and right lane change. Not all states can transition to all other states directly. Apparently, there's no way to make a left lane change if it's aleady in the left most lane; otherwise, it will have to cross the double yellow line and that's very dangerous. Also, if it's in the left most lane, it's not possible to be in the right most lane in the next time step.

In my implementation, the car constantly check the (potential) time to catch up with the cars in front for all the lanes. If a left lane change or right lane change can potentially increase the time, and it's safe to do so, then it will make a lane change. The creteria for safety is defined as "no car in parallel positions". I disabled double lane change because it sometimes result in some problems.

## Path Generation
In this part, the goal is to generate trajectories that the car can follow. One important requirement is that the trajectories generated should be smooth enough, otherwise the jerk will exceed the maximum allowed value. 

I used the points left in the last time step and a couple of new points obtained from behavior planning as pivots. These pivots are input into the "spline" lib to generate a smooth trajectory. In this step, I used coordinates transformation to make the work easier. The transformation was applied both between car fixed coordinate system and world coordinate system, and between world coordinate system and Frenet coordinate system. The code follows what's shown to us in the help video.
